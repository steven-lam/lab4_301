`timescale 1ns / 1ps
/*********************************************************
* Class:          CECS 301 T-Th 2:00PM
* Designer:       Thien Tran; Steven Lam
* Instructor:     R. W. Allison
*
* Project Number: 4 
* Module Name:    led_clk 
* Project Name:   Memory & Display Controllers
*
* Description: 	 A 250Hz clock used to time multiplex 
*                  the common anode inputs to seven segment 
*                  display.
*
*
*************************************************************/
module led_clk(CLK,RESET,led_clk);
    input CLK, RESET;
    output reg led_clk;
    integer i;
	always @ (posedge CLK or posedge RESET) begin
		// if statement that resets the clk when reset is pushed
		if (RESET == 1'b1) begin
			i = 0;
			led_clk = 0;
		end
		// got a clock, so increment the counter and
		// test to see if half a period has elapsed
		else begin
			i = i + 1;
			  // 100,000 because of [(Incoming Freq / Outgoing Freq) / 2 ] 
           // - [(50MHz/Outgoing Frq)/2] = 1
			  if (i >= 100000) begin
				  led_clk = ~led_clk;
				  i=0; // resets the i because the clk has already pulsed at 1 sec
				end // if
		end // else
	end // always


endmodule
