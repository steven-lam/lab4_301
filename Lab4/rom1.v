`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CSULB_CECS_Dept
// Engineer: Steven Lam; Luis Orozco
//
// Create Date:    15:30:04 05/03/2013 
// Design Name:    Lab9
// Module Name:    rom1 
//
// Description:    Used to store 250 hex digits, starting with FFh @ address 00h, etc
//
//////////////////////////////////////////////////////////////////////////////////
module rom1(
    input clk,
    input [7:0] addr,
    output [7:0] data
    );

//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
rom_256x8 dut (
  .clka(clk), // input clka
  .wea(wea), // input [0 : 0] wea
  .addra(addr), // input [7 : 0] addra
  .dina(dina), // input [7 : 0] dina
  .douta(data) // output [7 : 0] douta
);
// INST_TAG_END ------ End INSTANTIATION Template ---------

endmodule
