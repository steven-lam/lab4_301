`timescale 1ns / 1ps
/*********************************************************
* Class:          CECS 301 T-Th 2:00PM
* Designer:       Thien Tran; Steven Lam
* Instructor:     R. W. Allison
*
* Project Number: 4 
* Module Name:    ad_mux 
* Project Name:   Memory & Display Controllers
*
*
* Description: This is a 4 to 1 mux that selects between 
*              ad_hi, ad_lo, d_hi, and d_lo
*
*
*************************************************************/
module ad_mux(seg_sel, a_hi, a_lo, d_hi, d_lo, ad_out);
	 input  [1:0] seg_sel;
    input  [3:0]    a_hi;
    input  [3:0]    a_lo;
    input  [3:0]    d_hi;
    input  [3:0]    d_lo;
    output [3:0]  ad_out;
	 reg    [3:0]  ad_out;
	 
	 always @ (seg_sel, ad_out, a_hi, a_lo, d_hi, d_lo) begin
      // Case statement to turn on a specific anode when for specific select modes
		case (seg_sel)
			2'b00: ad_out = a_hi; // Data for a3
			2'b01: ad_out = a_lo; // Data for a2
			2'b10: ad_out = d_hi; // Data for a1
			2'b11: ad_out = d_lo; // Data for a0
         default: ad_out = 4'b0000; // Default set to all zeros
		endcase
	 end
endmodule