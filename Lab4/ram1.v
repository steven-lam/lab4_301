`timescale 1ns / 1ps
/*********************************************************
* Class:          CECS 301 T-Th 2:00PM
* Designer:       Thien Tran; Steven Lam
* Instructor:     R. W. Allison
*
* Project Number: 4 
* Module Name:    Ram1
* Project Name:   Memory & Display Controllers
* Rev. Date:      10/05/2013 2:20AM-2:57AM 4:46PM
*
* Purpose:        The purpose of this module is to store 
*                 data from the global data in inputs.
*
* Comments: 
*************************************************************/
module ram1(
   input          clk, // clock input
	input           we, // we? input
   input  [7:0]  addr, // 8 bit address input
	input  [15:0]  din,  // 16 bit data input
	output [15:0] dout  // 16 bit data output
   );
	
	//----------- Begin Cut here for INSTANTIATION Template ---// INST_TAG
ram_256x16 dut (
	.clka(clk),
	.wea(we),      // Bus [0 : 0] 
	.addra(addr),  // Bus [7 : 0] 
	.dina(din),    // Bus [15 : 0] 
	.douta(dout)); // Bus [15 : 0] 

// INST_TAG_END ------ End INSTANTIATION Template ---------
endmodule
