<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="clk_in" />
        <signal name="XLXN_14" />
        <signal name="XLXN_15" />
        <signal name="m0" />
        <signal name="m1" />
        <signal name="RESET" />
        <signal name="XLXN_16" />
        <signal name="XLXN_20(1:0)" />
        <signal name="a(3:0)" />
        <signal name="XLXN_21(3:0)" />
        <signal name="data(7:0)">
        </signal>
        <signal name="addr(7:0)">
        </signal>
        <signal name="addr(7:4)" />
        <signal name="addr(3:0)" />
        <signal name="data(7:4)" />
        <signal name="data(3:0)" />
        <signal name="ca" />
        <signal name="cb" />
        <signal name="cc" />
        <signal name="cd" />
        <signal name="ce" />
        <signal name="cf" />
        <signal name="cg" />
        <port polarity="Input" name="clk_in" />
        <port polarity="Input" name="m0" />
        <port polarity="Input" name="m1" />
        <port polarity="Input" name="RESET" />
        <port polarity="Output" name="a(3:0)" />
        <port polarity="Output" name="ca" />
        <port polarity="Output" name="cb" />
        <port polarity="Output" name="cc" />
        <port polarity="Output" name="cd" />
        <port polarity="Output" name="ce" />
        <port polarity="Output" name="cf" />
        <port polarity="Output" name="cg" />
        <blockdef name="quad_clk">
            <timestamp>2013-5-1T0:10:12</timestamp>
            <rect width="256" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
        </blockdef>
        <blockdef name="addr_seqr">
            <timestamp>2013-5-1T0:11:25</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
        </blockdef>
        <blockdef name="led_clk">
            <timestamp>2013-5-1T0:12:53</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
        </blockdef>
        <blockdef name="led_controller">
            <timestamp>2013-5-1T0:14:16</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
        </blockdef>
        <blockdef name="hex_to_7seg">
            <timestamp>2013-5-1T0:16:45</timestamp>
            <rect width="256" x="64" y="-448" height="448" />
            <line x2="0" y1="-416" y2="-416" x1="64" />
            <rect width="64" x="0" y="-428" height="24" />
            <line x2="384" y1="-416" y2="-416" x1="320" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
        </blockdef>
        <blockdef name="rom1">
            <timestamp>2013-5-3T22:34:45</timestamp>
            <rect width="256" x="64" y="-128" height="128" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="384" y1="-96" y2="-96" x1="320" />
            <rect width="64" x="320" y="-108" height="24" />
        </blockdef>
        <blockdef name="bufg">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="0" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="128" y1="0" y2="-32" x1="64" />
            <line x2="128" y1="-32" y2="-32" x1="224" />
            <line x2="64" y1="-32" y2="-32" x1="0" />
        </blockdef>
        <blockdef name="ad_mux">
            <timestamp>2013-5-6T21:19:24</timestamp>
            <rect width="256" x="64" y="-320" height="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <rect width="64" x="0" y="-300" height="24" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <rect width="64" x="0" y="-236" height="24" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-172" height="24" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <rect width="64" x="0" y="-108" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <rect width="64" x="320" y="-300" height="24" />
        </blockdef>
        <block symbolname="quad_clk" name="XLXI_1">
            <blockpin signalname="XLXN_16" name="clk_in" />
            <blockpin signalname="RESET" name="RESET" />
            <blockpin signalname="m1" name="m1" />
            <blockpin signalname="m0" name="m0" />
            <blockpin signalname="XLXN_14" name="clk_out" />
        </block>
        <block symbolname="addr_seqr" name="XLXI_2">
            <blockpin signalname="XLXN_14" name="CLK" />
            <blockpin signalname="RESET" name="RESET" />
            <blockpin signalname="addr(7:0)" name="addr(7:0)" />
        </block>
        <block symbolname="led_clk" name="XLXI_3">
            <blockpin signalname="XLXN_16" name="CLK" />
            <blockpin signalname="RESET" name="RESET" />
            <blockpin signalname="XLXN_15" name="led_clk" />
        </block>
        <block symbolname="led_controller" name="XLXI_4">
            <blockpin signalname="XLXN_15" name="CLK" />
            <blockpin signalname="RESET" name="RESET" />
            <blockpin signalname="a(3:0)" name="a(3:0)" />
            <blockpin signalname="XLXN_20(1:0)" name="seg_sel(1:0)" />
        </block>
        <block symbolname="hex_to_7seg" name="XLXI_5">
            <blockpin signalname="XLXN_21(3:0)" name="hex(3:0)" />
            <blockpin signalname="ca" name="ca" />
            <blockpin signalname="cb" name="cb" />
            <blockpin signalname="cc" name="cc" />
            <blockpin signalname="cd" name="cd" />
            <blockpin signalname="ce" name="ce" />
            <blockpin signalname="cf" name="cf" />
            <blockpin signalname="cg" name="cg" />
        </block>
        <block symbolname="rom1" name="XLXI_8">
            <blockpin signalname="XLXN_16" name="clk" />
            <blockpin signalname="addr(7:0)" name="addr(7:0)" />
            <blockpin signalname="data(7:0)" name="data(7:0)" />
        </block>
        <block symbolname="bufg" name="XLXI_9">
            <blockpin signalname="clk_in" name="I" />
            <blockpin signalname="XLXN_16" name="O" />
        </block>
        <block symbolname="ad_mux" name="XLXI_10">
            <blockpin signalname="XLXN_20(1:0)" name="seg_sel(1:0)" />
            <blockpin signalname="addr(7:4)" name="a_hi(3:0)" />
            <blockpin signalname="addr(3:0)" name="a_lo(3:0)" />
            <blockpin signalname="data(7:4)" name="d_hi(3:0)" />
            <blockpin signalname="data(3:0)" name="d_lo(3:0)" />
            <blockpin signalname="XLXN_21(3:0)" name="ad_out(3:0)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="2448" y="880" name="XLXI_8" orien="R0">
        </instance>
        <instance x="1392" y="1408" name="XLXI_4" orien="R0">
        </instance>
        <instance x="816" y="1408" name="XLXI_3" orien="R0">
        </instance>
        <branch name="clk_in">
            <wire x2="464" y1="848" y2="848" x1="448" />
        </branch>
        <instance x="1680" y="944" name="XLXI_2" orien="R0">
        </instance>
        <instance x="784" y="1072" name="XLXI_1" orien="R0">
        </instance>
        <iomarker fontsize="28" x="448" y="1040" name="m0" orien="R180" />
        <iomarker fontsize="28" x="448" y="912" name="RESET" orien="R180" />
        <iomarker fontsize="28" x="448" y="848" name="clk_in" orien="R180" />
        <instance x="464" y="880" name="XLXI_9" orien="R0" />
        <branch name="XLXN_14">
            <wire x2="1680" y1="848" y2="848" x1="1168" />
        </branch>
        <branch name="XLXN_15">
            <wire x2="1392" y1="1312" y2="1312" x1="1200" />
        </branch>
        <branch name="m0">
            <wire x2="784" y1="1040" y2="1040" x1="448" />
        </branch>
        <branch name="m1">
            <wire x2="784" y1="976" y2="976" x1="448" />
        </branch>
        <branch name="RESET">
            <wire x2="656" y1="912" y2="912" x1="448" />
            <wire x2="784" y1="912" y2="912" x1="656" />
            <wire x2="656" y1="912" y2="1152" x1="656" />
            <wire x2="1184" y1="1152" y2="1152" x1="656" />
            <wire x2="656" y1="1152" y2="1376" x1="656" />
            <wire x2="816" y1="1376" y2="1376" x1="656" />
            <wire x2="656" y1="1376" y2="1504" x1="656" />
            <wire x2="1328" y1="1504" y2="1504" x1="656" />
            <wire x2="1680" y1="912" y2="912" x1="1184" />
            <wire x2="1184" y1="912" y2="1152" x1="1184" />
            <wire x2="1392" y1="1376" y2="1376" x1="1328" />
            <wire x2="1328" y1="1376" y2="1504" x1="1328" />
        </branch>
        <branch name="XLXN_16">
            <wire x2="256" y1="736" y2="1312" x1="256" />
            <wire x2="816" y1="1312" y2="1312" x1="256" />
            <wire x2="768" y1="736" y2="736" x1="256" />
            <wire x2="768" y1="736" y2="848" x1="768" />
            <wire x2="784" y1="848" y2="848" x1="768" />
            <wire x2="2352" y1="736" y2="736" x1="768" />
            <wire x2="2352" y1="736" y2="784" x1="2352" />
            <wire x2="2448" y1="784" y2="784" x1="2352" />
            <wire x2="768" y1="848" y2="848" x1="688" />
        </branch>
        <branch name="XLXN_20(1:0)">
            <wire x2="1936" y1="1376" y2="1376" x1="1776" />
            <wire x2="1936" y1="1376" y2="1616" x1="1936" />
            <wire x2="2112" y1="1616" y2="1616" x1="1936" />
        </branch>
        <instance x="2736" y="2032" name="XLXI_5" orien="R0">
        </instance>
        <branch name="a(3:0)">
            <wire x2="3360" y1="1312" y2="1312" x1="1776" />
        </branch>
        <instance x="2112" y="1904" name="XLXI_10" orien="R0">
        </instance>
        <branch name="XLXN_21(3:0)">
            <wire x2="2736" y1="1616" y2="1616" x1="2496" />
        </branch>
        <branch name="addr(7:0)">
            <wire x2="2352" y1="848" y2="848" x1="2064" />
            <wire x2="2448" y1="848" y2="848" x1="2352" />
            <wire x2="2352" y1="848" y2="944" x1="2352" />
            <wire x2="3184" y1="944" y2="944" x1="2352" />
            <wire x2="3184" y1="944" y2="2160" x1="3184" />
            <wire x2="3184" y1="2160" y2="2240" x1="3184" />
            <wire x2="3184" y1="2240" y2="2672" x1="3184" />
        </branch>
        <branch name="addr(7:4)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2528" y="2160" type="branch" />
            <wire x2="2112" y1="1680" y2="1680" x1="1936" />
            <wire x2="1936" y1="1680" y2="2160" x1="1936" />
            <wire x2="2528" y1="2160" y2="2160" x1="1936" />
            <wire x2="3088" y1="2160" y2="2160" x1="2528" />
        </branch>
        <bustap x2="3088" y1="2160" y2="2160" x1="3184" />
        <bustap x2="3088" y1="2240" y2="2240" x1="3184" />
        <branch name="addr(3:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2528" y="2240" type="branch" />
            <wire x2="1968" y1="1744" y2="2240" x1="1968" />
            <wire x2="2528" y1="2240" y2="2240" x1="1968" />
            <wire x2="3088" y1="2240" y2="2240" x1="2528" />
            <wire x2="2112" y1="1744" y2="1744" x1="1968" />
        </branch>
        <branch name="data(3:0)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2528" y="2416" type="branch" />
            <wire x2="2112" y1="1872" y2="1872" x1="2032" />
            <wire x2="2032" y1="1872" y2="2416" x1="2032" />
            <wire x2="2528" y1="2416" y2="2416" x1="2032" />
            <wire x2="3392" y1="2416" y2="2416" x1="2528" />
            <wire x2="3392" y1="2416" y2="2464" x1="3392" />
            <wire x2="3392" y1="2464" y2="2464" x1="3328" />
        </branch>
        <branch name="data(7:0)">
            <wire x2="3232" y1="784" y2="784" x1="2832" />
            <wire x2="3232" y1="784" y2="2368" x1="3232" />
            <wire x2="3232" y1="2368" y2="2464" x1="3232" />
            <wire x2="3232" y1="2464" y2="2672" x1="3232" />
        </branch>
        <branch name="data(7:4)">
            <attrtext style="alignment:SOFT-BCENTER;fontsize:28;fontname:Arial" attrname="Name" x="2528" y="2320" type="branch" />
            <wire x2="2112" y1="1808" y2="1808" x1="2000" />
            <wire x2="2000" y1="1808" y2="2320" x1="2000" />
            <wire x2="2528" y1="2320" y2="2320" x1="2000" />
            <wire x2="3408" y1="2320" y2="2320" x1="2528" />
            <wire x2="3408" y1="2320" y2="2368" x1="3408" />
            <wire x2="3408" y1="2368" y2="2368" x1="3328" />
        </branch>
        <iomarker fontsize="28" x="3360" y="1312" name="a(3:0)" orien="R0" />
        <branch name="ca">
            <wire x2="3360" y1="1616" y2="1616" x1="3120" />
        </branch>
        <branch name="cb">
            <wire x2="3360" y1="1680" y2="1680" x1="3120" />
        </branch>
        <branch name="cc">
            <wire x2="3360" y1="1744" y2="1744" x1="3120" />
        </branch>
        <branch name="cd">
            <wire x2="3360" y1="1808" y2="1808" x1="3120" />
        </branch>
        <branch name="ce">
            <wire x2="3360" y1="1872" y2="1872" x1="3120" />
        </branch>
        <branch name="cf">
            <wire x2="3360" y1="1936" y2="1936" x1="3120" />
        </branch>
        <branch name="cg">
            <wire x2="3360" y1="2000" y2="2000" x1="3120" />
        </branch>
        <iomarker fontsize="28" x="3360" y="1616" name="ca" orien="R0" />
        <iomarker fontsize="28" x="3360" y="1680" name="cb" orien="R0" />
        <iomarker fontsize="28" x="3360" y="1744" name="cc" orien="R0" />
        <iomarker fontsize="28" x="3360" y="1808" name="cd" orien="R0" />
        <iomarker fontsize="28" x="3360" y="1872" name="ce" orien="R0" />
        <iomarker fontsize="28" x="3360" y="1936" name="cf" orien="R0" />
        <iomarker fontsize="28" x="3360" y="2000" name="cg" orien="R0" />
        <iomarker fontsize="28" x="448" y="976" name="m1" orien="R180" />
        <bustap x2="3328" y1="2368" y2="2368" x1="3232" />
        <bustap x2="3328" y1="2464" y2="2464" x1="3232" />
    </sheet>
</drawing>