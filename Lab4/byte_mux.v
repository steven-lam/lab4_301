`timescale 1ns / 1ps
/*********************************************************
* Class: CECS 301 T-Th 2:00PM
* Designer: Thien Tran; Steven Lam
* Instructor: R. W. Allison
*
* Project Number: 4 
* Module Name:    byte_mux 
* Project Name:   Memory & Display Controllers
* Rev. Date:      10/10/2013 4:35PM
*
* Purpose:        The purpose of this module is to select 
*                 between two different bytes of data 
*                 depending on the selects.
*
* Comments: 
*************************************************************/
module byte_mux(
   hi, // 8 bit hi input
	lo, // 8 bit lo input
	sel, // select input
	b_out // 8 bits/byte output
	);
   
	//---------------Input Ports-------------------
   input  [7:0]    hi, lo;
	input          sel;
	//---------------Output Ports------------------
	output [7:0] b_out;
	//-------------Internal Variables--------------
	reg    [7:0] b_out;
	
	//----------Code Starts Here-------------------
	always @ (sel, b_out, lo, hi) begin
	   case (sel)
		   1'b0: b_out = lo; // when select is 0, byte mux chooses lo
			1'b1: b_out = hi; // when select is 1, byte mux chooses hi
         default: b_out = 8'b00000000; // default b_out to all zeroes
	   endcase
	end


endmodule
