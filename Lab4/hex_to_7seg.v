`timescale 1ns / 1ps
/*************************************************************
* Class:      CECS 301 T-Th 2:00PM
* Designer:   Thien Tran; Steven Lam
* Instructor: R. W. Allison
*
* Project Number: 4 
* Module Name:    hex_to_7seg 
* Project Name:   Memory & Display Controllers
*
* Description: Behavioral model of a hex to 7-segment decoder 
*              with active-low outputs.
*
*************************************************************/
module hex_to_7seg(hex,ca,cb,cc,cd,ce,cf,cg);
    input [3:0] hex;
    output ca,cb,cc,cd,ce,cf,cg;
	 reg 	ca,cb,cc,cd,ce,cf,cg;
	
	 // Case Statement to output a 7-segment LED depending on the 
    // 4-bit hex input
	 always @ (hex) begin
	 case (hex)
		4'b0000: {ca,cb,cc,cd,ce,cf,cg} = 7'b0000001;
		4'b0001: {ca,cb,cc,cd,ce,cf,cg} = 7'b1001111;
		4'b0010: {ca,cb,cc,cd,ce,cf,cg} = 7'b0010010;
		4'b0011: {ca,cb,cc,cd,ce,cf,cg} = 7'b0000110;
		4'b0100: {ca,cb,cc,cd,ce,cf,cg} = 7'b1001100;
		4'b0101: {ca,cb,cc,cd,ce,cf,cg} = 7'b0100100;
		4'b0110: {ca,cb,cc,cd,ce,cf,cg} = 7'b0100000;
		4'b0111: {ca,cb,cc,cd,ce,cf,cg} = 7'b0001111;
		4'b1000: {ca,cb,cc,cd,ce,cf,cg} = 7'b0000000;
		4'b1001: {ca,cb,cc,cd,ce,cf,cg} = 7'b0001100;
		4'b1010: {ca,cb,cc,cd,ce,cf,cg} = 7'b0001000;
		4'b1011: {ca,cb,cc,cd,ce,cf,cg} = 7'b1100000;
		4'b1100: {ca,cb,cc,cd,ce,cf,cg} = 7'b0110001;
		4'b1101: {ca,cb,cc,cd,ce,cf,cg} = 7'b1000010;
		4'b1110: {ca,cb,cc,cd,ce,cf,cg} = 7'b0110000;
		4'b1111: {ca,cb,cc,cd,ce,cf,cg} = 7'b0111000;
		default: {ca,cb,cc,cd,ce,cf,cg} = 7'b1111111;
		endcase
	end
endmodule
