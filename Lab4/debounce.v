`timescale 1ns / 1ps
/*********************************************************
* Class:         CECS 301 T-Th 2:00PM
* Designer:      Thien Tran; Steven Lam
* Instructor: R. W. Allison
*
* Project Number: 4 
* Module Name:    debounce 
* Project Name:   Memory & Display Controllers
*
* Purpose:        This is the debounce module that outputs 
*                 a clock pulse for the addr_seq module. 
*                 The debounce module only outputs the clock 
*						pulse when it sees that all the reg's 
*                 (q9-q0) are 1's. 
*
* Comments: 
*************************************************************/
module debounce(
	 clk, // Clock Input
	 reset, // Reset Input
	 Din, // Data Input
	 Dout // Data Output
    );
	 
	 //---------------Input Ports-------------------
	 input Din, clk, reset;	 
	 //---------------Output Ports------------------
	 output Dout;	 
	 //-------------Internal Variables--------------
	 wire Dout;
	 reg q9,q8,q7,q6,q5,q4,q3,q2,q1,q0;
	 
	 //----------Code Starts Here-------------------
	 always @ (posedge clk or posedge reset)
		if (reset == 1'b1)
			{q9,q8,q7,q6,q5,q4,q3,q2,q1,q0} <= 10'b0;
		else begin
			// shift in the new sample that's on the Din input
			q9 <= q8; q8 <= q7; q7 <= q6; q6 <= q5; q5 <= q4;
			q4 <= q3; q3 <= q2; q2 <= q1; q1 <= q0; q0 <= Din;
		end // else

// create the debounced, one-shot output pulse
assign Dout = !q9 & q8 & q7 & q6 & q5 &
					q4 & q3 & q2 & q1 & q0;
endmodule
