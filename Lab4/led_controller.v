`timescale 1ns / 1ps
/*********************************************************
* Class:      CECS 301 T-Th 2:00PM
* Designer:   Thien Tran; Steven Lam
* Instructor: R. W. Allison
*
* Project Number: 4 
* Module Name:    led_controller 
* Project Name:   Memory & Display Controllers
*
* Description: This module generates the signals for the common 
*              anode input to the seven segment display and also 
*              generates the multiplexer select signals for 
*              multiplexing the address/data nibbles
*
*************************************************************/
module led_controller(CLK,RESET,a,seg_sel);
	input CLK, RESET;
	output reg [3:0] a;
	output reg [1:0] seg_sel;
	reg [1:0] Q,D;
	//Next State Logic
	always @(Q) begin
		case (Q)
			2'b00: D=2'b01; // If present state is 00, next state is 01
			2'b01: D=2'b10; // If present state is 01, next state is 10
			2'b10: D=2'b11; // If present state is 10, next state is 11
			2'b11: D=2'b00; // If present state is 11, next state is 00
			default: D=2'b00; // Default is next state will be 00
		endcase
	end
	//Next State Register
	always @ (posedge CLK or posedge RESET) begin
      // If reset is pushed, present state is 00
		if (RESET == 1'b1) begin
			Q <= 2'b00;
		end
		// Else present state is assigned next state
		else begin
			Q <= D;
		end
	end
	
	//Output Logic
	always @(Q) begin
		case (Q)
			2'b00: {a,seg_sel}=6'b0111_00; 
         // Turn on a3 and output select as 00 for add_mux
			2'b01: {a,seg_sel}=6'b1011_01; 
         // Turn on a2 and output select as 01 for add_mux
			2'b10: {a,seg_sel}=6'b1101_10; 
         // Turn on a1 and output select as 10 for add_mux
			2'b11: {a,seg_sel}=6'b1110_11; 
         // Turn on a0 and output select as 11 for add_mux
			default: {a,seg_sel}=6'b0111_00; 
         // Default turn on a3 and output select as 00 for add_mux
		endcase
	end
	
	
endmodule
