`timescale 1ns / 1ps
/*********************************************************
* Class:          CECS 301 T-Th 2:00PM
* Designer:       Thien Tran; Steven Lam
* Instructor:     R. W. Allison
*
* Project Number: 4 
* Module Name:    Clock 500Hz 
* Project Name:   Memory & Display Controllers
*
* Purpose:        The purpose of this module is to reduce 
*                 the 50 MHz clock to a 500 Hz clock
*
* Comments: 
*************************************************************/
module clock_500Hz(
   clk, // Clock Input
   reset, // Reset Input
   clk_out // Clock Output
   );	 
	
   //-----------Input Ports----------------
   input clk, reset;	 
   //-----------Output Ports---------------
   output clk_out;	 
   //------------Internal Variables--------
   reg clk_out;
   integer i;	
	
   //-------------Code Starts Here---------
	always @(posedge clk or posedge reset) begin
		if (reset == 1'b1) begin
			i = 0;
			clk_out = 0;
		end
		// got a clock, so increment the counter and
		// test to see if half a period has elapsed
		else begin
			i = i + 1;
			if (i >= 50000) begin // [50,000,000/500]/2 = 50,000
				clk_out = ~clk_out;
				i = 0;
			end // if
		end // always
		
	end // always
endmodule
