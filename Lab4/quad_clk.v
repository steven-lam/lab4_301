`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CSULB_CECS_Dept
// Engineer: Steven Lam; Luis Orozco
//
// Create Date:    17:27:09 04/30/2013 
// Design Name: 	 Lab9
// Module Name:    quad_clk 
//
// Description: This is a clk that reduces the 50MHz clock to 4 different clk speeds
//					 depending on the selects
//
//////////////////////////////////////////////////////////////////////////////////
module quad_clk(clk_in, RESET, clk_out, m1, m0);

	input clk_in, RESET, m1, m0 ;
	output reg clk_out;
	integer i;
	
	always @ (posedge clk_in or posedge RESET) begin
		//if statement that resets the clk when reset is pushed
		if (RESET == 1'b1) begin
			i = 0;
			clk_out = 0;
		end
		// got a clock, so increment the counter and
		// test to see if half a period has elapsed
		else begin
			i = i + 1;
		   case({m1,m0})
		     2'b00: 
			  //25,000,000 because of [(Incoming Freq / Outgoing Freq) / 2 ] - [(50MHz/Outgoing Frq)/2] = 1
			  if (i >= 25000000) begin
				  clk_out = ~clk_out;
				  i=0; //resets the i because the clk has already pulsed at 1 sec
				end
			  2'b01:
			  if (i >=5000000) begin
				  clk_out = ~clk_out;
			  	  i=0;
				end
		     2'b10:
			  if (i>=2500000) begin
				  clk_out = ~clk_out;
			  	  i=0;
				end
		     2'b11:
			  if (i>=1250000) begin
				  clk_out = ~clk_out;
				  i=0;
				end // if
			endcase //case
		end // else
	end // always


endmodule
