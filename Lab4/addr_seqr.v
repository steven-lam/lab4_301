`timescale 1ns / 1ps
/*********************************************************
* Class:          CECS 301 T-Th 2:00PM
* Designer:       Thien Tran; Steven Lam
* Instructor:     R. W. Allison
*
* Project Number: 4 
* Module Name:    addr_seqr 
* Project Name:   Memory & Display Controllers
*
*
* Description: This module creates the address for the ROM
*
*
**********************************************************/
module addr_seqr(CLK,RESET,addr);
	 input CLK;
    input RESET;
    output reg [7:0] addr;
   // Always block that begins on a clock pulse or reset
	always @ (posedge CLK or posedge RESET)begin
      // Sets address to 8'b00000000 if reset is pushed
		if(RESET==1'b1)begin
			addr=8'b00000000;
		end
		else begin
			addr=addr+1; // If reset isn't pushed, increment
                      // address by one
		end
	end
endmodule
