`timescale 1ns / 1ps
/*********************************************************
* Class:      CECS 301 T-Th 2:00PM
* Designer:   Thien Tran; Steven Lam
* Instructor: R. W. Allison
*
* Project Number: 4 
* Module Name:    Lab4 Top Level 
* Project Name:   Memory & Display Controllers
* Rev. Date:      10/05/2013 1:48AM-2:05AM 4:46PM-6:35PM
*
* Purpose:        The purpose of this module is to 
*                 interconnect all the verilog modules and
*                 connect the global inputs/outputs to the 
*                 local inputs/outputs.
*
* Comments: 
*************************************************************/
module Lab4_Top_Level(
	clock_50MHz, // clk_50MHz ram1 input
	reset,       // global reset input
	wr_pulse,    // Data debounce0 input
	addr_step,   // Data debounce1 input
   din,         // Data in for the RAM
	byte_sel,    // byte_mux input
	an,          // 4 bit anodes outputs
	ca, cb, cc, cd, ce, cf, cg // 7 segment LED ouputs
   );

   //-----------Input Ports----------------
   // Global Inputs
   input clock_50MHz, reset,wr_pulse, addr_step, byte_sel; 
   input [7:0] din; // 8 bit input for the RAM's data in; 
                    // Switches 7-0
   //-----------Output Ports---------------
   output [3:0] an;
   output ca, cb, cc, cd, ce, cf, cg;	
   //------------Internal Variables--------
   wire [3:0] an; // anodes
   wire ca, cb, cc, cd, ce, cf, cg; // 7 segments
	wire clk_out, led_clk_out, addr_debounce_out, 
        we_debounce_out, clk_buff; 
	wire [7:0] addr;    // 8 bit address output
	wire [3:0] ad_out;  // 4 bit address/data output to hex
	wire [1:0] seg_sel; // 2 bit segment selection
   wire [7:0] b_out;   // 8 bit data out from byte_mux
   wire [3:0] ram_ad_hi, ram_ad_lo; // RAM data out [15:8]
   wire [3:0] ram_d_hi, ram_d_lo; // RAM data out [7:0]
	
	//****************Code Starts Here********************
	
   BUFG clk_Buffer (.I(clock_50MHz), .O(clk_buff));
   
	//---------------clock 500Hz module-------------------
	clock_500Hz clk_module(clk_buff, reset, clk_out);
   // clk_out --> address debounce module
	
	//-----------------led clock module-------------------
	led_clk LED_clk_module(clk_buff, reset, led_clk_out);
	// led_clk_out --> LED debounce module
	
	//------------------debounce module-------------------
	debounce we_debounce_module(clk_out, reset, wr_pulse, 
                               we_debounce_out); 
	// we_debounce_out --> RAM module
	
	//----------address debounce module-------------------
	debounce addr_de_module(clk_out, reset, addr_step, 
                           addr_debounce_out);
	// addr_debounce_out --> addr_seqr module
	
	//-----------------addr seqr module-------------------
	addr_seqr Ad_seq_module(addr_debounce_out, reset, addr);
	// addr --> ram1
	// addr --> hi, lo address ad_mux
	
	//-----------------------ram module-------------------
	ram1 ram_module(clk_buff, we_debounce_out, addr, 
                  {8'b11111111,din}, {ram_ad_hi, ram_ad_lo, 
                   ram_d_hi, ram_d_lo}); 
	// dout --> byte_mux          
	
	//------------------byte_mux module-------------------
	byte_mux b_mux_module({ram_ad_hi, ram_ad_lo}, addr, 
                         byte_sel, b_out); 
	// b_out --> hi, lo data ad_mux
   // {ram_ad_hi, ram_ad_lo} is the 8 bits for byte_mux hi
   // addr is the 8 bits for byte mux lo
	
	//--------------------ad_mux module-------------------
	ad_mux ad_module(seg_sel, b_out[7:4], b_out[3:0], 
                    ram_d_hi, ram_d_lo, ad_out); 
	// ad_out --> hex_to_7seg
   // b_out[7:4] gets assigned to a3 when select is 00;
   // b_out[3:0] gets assigned to a2 when select is 01;
   // ram_d_hi gets assigned to a1 when select is 10; 
   // ram_d_lo gets assigned to a0 when select is 11;
	
	//------------led controller module-------------------
	led_controller LED_ctr_module(led_clk_out, reset, an, 
                                 seg_sel);
	// seg_sel --> ad_mux
	
	//---------------hex_to_7seg module-------------------
	hex_to_7seg seg_module(ad_out, ca, cb, cc, cd, ce, cf, cg);
	
endmodule

