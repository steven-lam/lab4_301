`timescale 1ns / 1ps
/*********************************************************
* Class:          CECS 301 T-Th 2:00PM
* Designer:       Thien Tran; Steven Lam
* Instructor:     R. W. Allison
*
* Project Number: 4 
* Module Name:    Clock 500Hz 
* Project Name:   Memory & Display Controllers
*
* Purpose:        A buffer to distribute the global 50MHz 
*                 clock to other modules.  
*
* Comments: 
*************************************************************/
module BUFG(
   input  I,
   output O
    );


endmodule
